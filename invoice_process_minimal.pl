% after converting csv into prolog database (and putting into file
% new_invoices99), use:
%
% :-ensure_loaded(new_invoices99).


/*
 *
 *
 *
 *
 * take every invoice line item we have and categorise it as either "new customer" (first date the company bought anything), �new product� (the customer has bought something before this date, but this is the first time they�re buying this), or �existing product� (the customer already owns this).
 *
 *
 *
 *
 *
 *
 */


%
% bought (Who, What, When).
:- dynamic bought/3.

:-dynamic invoice_updated/18.

main:-

	% only need this if runninng more than once
%	retractall(bought(_,_,_)),retractall(invoice_updated(_Product,
%	_Date, _C, _D, _Source, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O,
%	_P, _Company,_)),!,

	% when converting csv file- each row becomes an invoice/17 predicate
	invoice(Product, NewDate, C, D, Source, F, G, H, I, J, K, L, M, N, O, P, Company),

	check_if_bought(Product, NewDate, C, D, Source, F, G, H, I, J, K, L, M, N, O, P, Company),false

	.


write_result:-

	open('updated_invoices99.pl',write,Out),with_output_to(Out,\+ get_asserts),close(Out)

	.


get_asserts:-

	invoice_updated(Product, NewDate, C, D, Source, F, G, H, I, J, K, L, M, N, O, P, Company,NewColumn),
	write_canonical(invoice_updated(Product, NewDate, C, D, Source, F, G, H, I, J, K, L, M, N, O, P, Company,NewColumn)),nl,fail

	.



% deterministic
check_if_bought(Product, NewDate, C, D, Source, F, G, H, I, J, K, L, M, N, O, P, Company):-

	(

	         bought(Company,Product,_OldDate1),assert(invoice_updated(Product, NewDate, C, D, Source, F, G, H, I, J, K, L, M, N, O, P, Company,'Existing Product'))

    ;

		 bought(Company,_OldProduct,_OldDate2),assert(bought(Company,Product,NewDate)),assert(invoice_updated(Product, NewDate, C, D, Source, F, G, H, I, J, K, L, M, N, O, P, Company,'New Product'))


    ;

		assert(bought(Company,Product,NewDate)), assert(invoice_updated(Product, NewDate, C, D, Source, F, G, H, I, J, K, L, M, N, O, P, Company,'New Customer'))

	)


    ,!

	.


test(Company):-
	invoice_updated(_Product, _Date, _C, _D, _Source, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O, _P, Company,X)
	,nl,write(X),nl

	.


test2(Company):-

	findall(X,invoice_updated(_Product, _Date, _C, _D, _Source, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O, _P, Company,X),All)

	,nl,write(All),nl

	.



test3(Company):-

	findall(X,invoice_updated(_Product, _Date, _C, _D, _Source, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O, _P, Company,'New Product'),All)

	,length(All,L)
	,nl,write('new product count='),write(L),nl

	,
	findall(X,invoice_updated(_Product, _Date, _C, _D, _Source, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O, _P, Company,'New Customer'),All1)

	,length(All1,L1)
	,nl,write('new customer count='),write(L1),nl


	.


