(SWI) Prolog implementation of request to process Redgate invoices.

Run main/0 to obtain results (in memory).

Run write_results/0 to print results to file. (It's easy to convert back into a csv file when needed).

I used the code here to convert  csv <-> prolog database:

https://prologsource.wordpress.com/2007/09/21/csv2swiprolog/

but there are others too (SWI has its own library I think).